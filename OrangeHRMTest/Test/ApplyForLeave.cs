﻿using NUnit.Framework;
using OrangeHRMTest.HelperClass;
using OrangeHRMTest.Test.Setup;

namespace OrangeHRMTest.Test.Positive
{
    public class ApplyForLeave : BaseTest
    {
        [Test]
        public void GivenValidInformation_WhenApplyForCasualLeave_ThenConfirmLeaveApplication()
        {
            LeaveApplicationHelper.LeaveApplicationSteps("Casual Leave", "Wed, 13 Jan 2021", "Wed, 14 Jan 2021", "I just want to rest for a while.");

            Assert.IsTrue(OrangeHRM.ApplyForLeavePage.SuccessMessage.Displayed);
        }

        [Test]
        public void GivenDatesThatWhillResultsInLeaveOverlap_WhenApplyingForCasualLeave_ThenConfirmOverlappingLeaveModalIsDisplayed()
        {
            LeaveApplicationHelper.LeaveApplicationSteps("Casual Leave", "Wed, 13 Jan 2021", "Wed, 14 Jan 2021", "I just want to rest for a while.");

            Assert.IsTrue(OrangeHRM.ApplyForLeavePage.LeaveOverlappingModal.Displayed);
        }

        [Test]
        public void GivenWeekendDates_WhenApplyingForLeave_ThenConfirmAValidationMessageIsDisplayed()
        {
            LeaveApplicationHelper.LeaveApplicationSteps("Casual Leave", "Sat, 30 May 2020", "Sat, 30 May 2020", "I just want to rest for a while.");

            Assert.IsTrue(OrangeHRM.ApplyForLeavePage.NotWorkingMessage.Displayed);
        }

        [Test]
        public void GivenEmptyleaveInformation_WhenApplyingForLeave_ThenConfirmValidationMessageShow()
        {
            OrangeHRM.LoginPage.LoginButton.Click();
            OrangeHRM.BasePage.LeaveLink.Click();
            OrangeHRM.BasePage.ApplyForLeaveLink.Click();
            OrangeHRM.ApplyForLeavePage.WaitForLeavePage();
            OrangeHRM.ApplyForLeavePage.ApplyButton.Click();

            Assert.AreEqual(3, OrangeHRM.ApplyForLeavePage.RequiredErrorMessage.Count);
        }
    }
}
