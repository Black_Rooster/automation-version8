﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;

namespace OrangeHRMTest.Test.Setup
{
    [TestFixture]
    public class BaseTest
    {
        private IWebDriver _driver;
     
        private readonly int timeoutSeconds = 15;

        [SetUp]
        public void DriverInitialize()
        {
            _driver = new ChromeDriver();
            _driver.Manage().Window.Maximize();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeoutSeconds);
            OrangeHRM.PageInitialize(_driver);
        }

        [TearDown]
        public void DriverShutdown()
        {
            _driver.Dispose();
        }
    }
}
