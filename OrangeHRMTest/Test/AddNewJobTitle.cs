﻿using NUnit.Framework;
using OpenQA.Selenium;
using OrangeHRMTest.Test.Setup;

namespace OrangeHRMTest.Test.Positive
{
    [TestFixture]
    public class AddNewJobTitle: BaseTest
    {
        private readonly string _jobTitle = "Software Developer";

        [SetUp]
        public void OpenAddJobTitleModal()
        {
            OrangeHRM.LoginPage.LoginButton.Click();
            OrangeHRM.BasePage.AdminLink.Click();
            OrangeHRM.BasePage.JobMenu.Click();
            OrangeHRM.BasePage.JobTitleLink.Click();
            OrangeHRM.JobTitlePage.AddJobTitleButton.Click();
            OrangeHRM.JobTitlePage.WaitForAddJobTitleModal();
        }

        [Test]
        public void GivenValidJobTitleInformation_WhenAddingNewJobTitle_ThenConfirmNewJobTitleIsAdded()
        {
            OrangeHRM.JobTitlePage.JobTitleField.SendKeys(_jobTitle);
            OrangeHRM.JobTitlePage.JobDescriptionField.SendKeys("People that can be mad at themselves for spending 3 - 4 hours trying fix something that isn't broke, they are only missing a semicolon");
            OrangeHRM.JobTitlePage.JobNoteField.SendKeys("I am a programmer I have no life");
            OrangeHRM.JobTitlePage.SaveButton.Click();

            Assert.IsTrue(OrangeHRM.JobTitlePage.Contains(_jobTitle).Displayed);
        }

        [Test]
        public void GivenEmptyJobTitleName_WhenAddingNewJobTitle_ThenConfirmValidationMessageIsDisplayed()
        {
            OrangeHRM.JobTitlePage.JobTitleField.SendKeys(string.Empty);
            OrangeHRM.JobTitlePage.SaveButton.Click();

            Assert.IsTrue(OrangeHRM.JobTitlePage.ErrorMessage.Displayed);
        }

        [Test]
        public void GivenValidJobTitleInformation_WhenCancellingAttemptToAddNewJobTitle_ThenConfirmJobTitleIsNotAdded()
        {
            var jobTitle = "Software Analyst";

            OrangeHRM.JobTitlePage.JobTitleField.SendKeys(jobTitle);
            OrangeHRM.JobTitlePage.JobDescriptionField.SendKeys("I am testing cancel");
            OrangeHRM.JobTitlePage.JobNoteField.SendKeys("I am a programmer I have no life");
            OrangeHRM.JobTitlePage.CancelButton.Click();

            var actual = Assert.Throws<NoSuchElementException>(() => OrangeHRM.JobTitlePage.Contains(jobTitle));

            Assert.AreEqual("Element not found", actual.Message);
        }

        [OneTimeTearDown]
        public void RemoveTheAddedJobTitle()
        {
            OrangeHRM.JobTitlePage.GetJobTitleCheckbox(_jobTitle).Click();
            OrangeHRM.JobTitlePage.ListOptionButton.Click();
            OrangeHRM.JobTitlePage.DeleteSelectOption.Click();
            OrangeHRM.JobTitlePage.ConfirmDelete.Click();
        }
    }
}
