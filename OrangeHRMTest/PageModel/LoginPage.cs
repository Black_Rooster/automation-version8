﻿using OpenQA.Selenium;

namespace OrangeHRMTest.PageModel
{
    public class LoginPage : BasePage
    {
        public LoginPage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement UsernameField => Driver.FindElement(By.Id("txtUsername"));

        public IWebElement PasswordField => Driver.FindElement(By.Id("txtPassword"));

        public IWebElement LoginButton => Driver.FindElement(By.Id("btnLogin"));
    }
}
