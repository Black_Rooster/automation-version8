﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;

namespace OrangeHRMTest.PageModel
{
    public class BasePage
    {
        public IWebDriver Driver;
        public WebDriverWait DriverWait;

        public BasePage(IWebDriver driver)
        {
            Driver = driver;
            DriverWait = new WebDriverWait(Driver, TimeSpan.FromSeconds(10));
        }

        public IWebElement AdminMenu => Driver.FindElement(By.XPath("//div[@id='menu-content']//ul"));

        public IWebElement AdminLink => AdminMenu.FindElement(By.XPath("//li[@id='menu_admin_viewAdminModule']/a"));

        public IWebElement JobMenu => Driver.FindElement(By.XPath("//li[@id='menu_admin_Job']/a"));

        public IWebElement JobTitleLink => Driver.FindElement(By.Id("menu_admin_viewJobTitleList"));

        public IWebElement LeaveLink => Driver.FindElement(By.XPath("//li[@id='menu_leave_viewLeaveModule']/a"));

        public IWebElement ApplyForLeaveLink => Driver.FindElement(By.Id("menu_leave_applyLeave"));
    }
}
