﻿using OpenQA.Selenium;
using System;

namespace OrangeHRMTest.PageModel
{
    public class JobTitlePage : BasePage
    {
        public JobTitlePage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement AddJobTitleButton => Driver.FindElement(By.XPath("//div[@id='jobTitlesDiv']/div/a"));

        public IWebElement AddJobTitleModal => Driver.FindElement(By.Id("modal1"));

        public IWebElement JobTitleField => AddJobTitleModal.FindElement(By.Id("jobTitleName"));

        public IWebElement JobDescriptionField => AddJobTitleModal.FindElement(By.Id("jobDescription"));

        public IWebElement JobNoteField => AddJobTitleModal.FindElement(By.Id("note"));

        public IWebElement SaveButton => AddJobTitleModal.FindElement(By.LinkText("SAVE"));

        public IWebElement CancelButton => AddJobTitleModal.FindElement(By.LinkText("CANCEL"));

        public IWebElement ListOptionButton => Driver.FindElement(By.XPath("//th[@class='list-options']/a"));

        public IWebElement DeleteSelectOption => Driver.FindElement(By.LinkText("Delete Selected"));

        public IWebElement ErrorMessage => AddJobTitleModal.FindElement(By.XPath("//span[text()='Required']"));

        public IWebElement ConfirmDelete => Driver.FindElement(By.LinkText("Yes, Delete".ToUpper()));

        public void WaitForAddJobTitleModal()
        {
            DriverWait.Until(result => AddJobTitleModal.Displayed);
        }

        public IWebElement Contains(string jobTitle)  
        {
            try
            {
                return Driver.FindElement(By.XPath("//div[@id='jobTitlesDiv']//table//tbody//tr//td[2]//span[text()='" + jobTitle + "']"));
            }
            catch (NoSuchElementException)
            {
                throw new NoSuchElementException("Element not found");
            }
        }

        public IWebElement GetJobTitleCheckbox(string jobTitle)
        {
            return Driver.FindElement(By.XPath("//tr[.//td[2]//span[text()='" + jobTitle + "']]//label"));
        }
    }
}
