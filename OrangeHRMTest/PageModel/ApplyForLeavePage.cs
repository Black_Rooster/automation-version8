﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace OrangeHRMTest.PageModel
{
    public class ApplyForLeavePage : BasePage
    {
        public ApplyForLeavePage(IWebDriver driver) : base(driver)
        {
        }

        public IWebElement LeaveTypeSelect => Driver.FindElement(By.XPath("//div[@id='leaveType_inputfileddiv']//input"));

        public IWebElement StartDate => Driver.FindElement(By.Id("from"));

        public IWebElement EndDate => Driver.FindElement(By.Id("to"));

        public IWebElement ApplyButton => Driver.FindElement(By.XPath("//button[@type='submit']"));

        public IWebElement SuccessMessage => Driver.FindElement(By.XPath("//div[text()='Successfully Submitted']"));

        public IWebElement NotWorkingMessage => Driver.FindElement(By.Id("toast-container"));

        public IWebElement LeaveOverlappingModal => Driver.FindElement(By.Id("application_balance_modal"));

        public IList<IWebElement> RequiredErrorMessage => Driver.FindElements(By.XPath("//span[text()='Required']"));

        public IWebElement CommetTextarea => Driver.FindElement(By.Id("comment"));

        public IWebElement GetLeaveTypeOption(string optionName)
        {
            return LeaveTypeSelect.FindElement(By.XPath("//li//span[text()='" + optionName + "']"));
        }

        public void WaitForLeavePage()
        {
            DriverWait.Until(resu => Driver.FindElement(By.Id("applyLeaveDiv")).Displayed);
        }

        public void WaitForLeaveBalanceLink()
        {
            DriverWait.Until(result => Driver.FindElement(By.LinkText("Check Leave Balance")).Displayed);
        }
    }
}
