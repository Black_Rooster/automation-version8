﻿namespace OrangeHRMTest.HelperClass
{
    public static class LeaveApplicationHelper
    {
        public static void LeaveApplicationSteps(string leaveType, string startDate, string endDate, string comment)
        {
            OrangeHRM.LoginPage.LoginButton.Click();
            OrangeHRM.BasePage.LeaveLink.Click();
            OrangeHRM.BasePage.ApplyForLeaveLink.Click();
            OrangeHRM.ApplyForLeavePage.WaitForLeavePage();
            OrangeHRM.ApplyForLeavePage.LeaveTypeSelect.Click();
            OrangeHRM.ApplyForLeavePage.GetLeaveTypeOption(leaveType).Click();
            OrangeHRM.ApplyForLeavePage.WaitForLeaveBalanceLink();
            OrangeHRM.ApplyForLeavePage.StartDate.SendKeys(startDate);
            OrangeHRM.ApplyForLeavePage.EndDate.Click();
            OrangeHRM.ApplyForLeavePage.EndDate.Clear();
            OrangeHRM.ApplyForLeavePage.EndDate.SendKeys(endDate);
            OrangeHRM.ApplyForLeavePage.CommetTextarea.SendKeys(comment);
            OrangeHRM.ApplyForLeavePage.ApplyButton.Click();
        }
    }
}
