﻿using OpenQA.Selenium;
using OrangeHRMTest.PageModel;

namespace OrangeHRMTest
{
    public static class OrangeHRM
    {
        public static BasePage BasePage;
        public static LoginPage LoginPage;
        public static JobTitlePage JobTitlePage;
        public static ApplyForLeavePage ApplyForLeavePage;

        public static void PageInitialize(IWebDriver driver)
        {
            driver.Navigate().GoToUrl("https://orangehrm-demo-6x.orangehrmlive.com/");
            BasePage = new BasePage(driver);
            LoginPage = new LoginPage(driver);
            JobTitlePage = new JobTitlePage(driver);
            ApplyForLeavePage = new ApplyForLeavePage(driver);
        }
    }
}
